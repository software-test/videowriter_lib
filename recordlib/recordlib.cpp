#include "recordlib.h"
#include <QDebug>
#include  <QImage>

using namespace cv;
Recordlib::Recordlib()
{
}

int Recordlib::recording(bool checked)
{
    if(checked==false)
    {
        qDebug()<<"record off";
        oVideoWriter.release();
        mVideoCap.release();
        return 1;
    }
    mVideoCap.open(0);
    if(mVideoCap.isOpened())
    {
        int frame_width = static_cast<int>(mVideoCap.get(CAP_PROP_FRAME_WIDTH)); //get the width of frames of the video
        int frame_height = static_cast<int>(mVideoCap.get(CAP_PROP_FRAME_HEIGHT)); //get the height of frames of the video
        int frame_rate = static_cast<int>(mVideoCap.get(CAP_PROP_FPS));
        Size frame_size(frame_width, frame_height);
        oVideoWriter.open("/home/htic/qt_opencv.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), frame_rate, frame_size, true);
        while(checked)
        {
            mVideoCap>>mFrame;

            if(!mFrame.empty() )
            {
                oVideoWriter.write(mFrame);
            }

            waitKey(10);

        }

        return 1;
    }
    else
    {
        qDebug()<<"cap not opened";
        return 0;
    }
    return 1;
}



