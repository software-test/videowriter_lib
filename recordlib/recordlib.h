#ifndef RECORDLIB_H
#define RECORDLIB_H

#include <QImage>
#include "recordlib_global.h"
#include <opencv2/opencv.hpp>


class RECORDLIB_EXPORT Recordlib
{
public:
    Recordlib();
    int recording(bool checked);
    qImage videowrite();
    cv::Mat mFrame;  //Opencv image
    cv::VideoCapture mVideoCap; //video capture
    cv::VideoWriter oVideoWriter;

};

#endif // RECORDLIB_H
