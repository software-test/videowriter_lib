#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

#include "../recordlib/recordlib.h"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->record_button->setCheckable(true);

}

MainWindow::~MainWindow()
{
    delete ui;
}





void MainWindow::on_record_button_toggled(bool checked)
{

    rlib.recording(checked);

}

