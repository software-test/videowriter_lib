QT -= gui

TEMPLATE = lib
DEFINES += RECORDLIB_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    recordlib.cpp

HEADERS += \
    recordlib_global.h \
    recordlib.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

LIBS += `pkg-config opencv4 --libs` -luvc
INCLUDEPATH += \
    /usr/local/include/opencv4
