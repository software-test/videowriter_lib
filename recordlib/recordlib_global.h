#ifndef RECORDLIB_GLOBAL_H
#define RECORDLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(RECORDLIB_LIBRARY)
#  define RECORDLIB_EXPORT Q_DECL_EXPORT
#else
#  define RECORDLIB_EXPORT Q_DECL_IMPORT
#endif

#endif // RECORDLIB_GLOBAL_H
